
package com.galvanize;

public class CallingCard {

    private int centsPerMin;
    private int minutesOnCard;

    // when instantiated CallingCard will be passed an int centsPerMin
    public CallingCard(int centsPerMin) {
        this.centsPerMin = centsPerMin;
        this.minutesOnCard = 0;
    }

    public void addDollars(int dollarAmount) {
        setMinutesOnCard(dollarAmount);
    }

    public void useMinutes(int minutes) {
        this.minutesOnCard -= minutes;
    }

    public int getRemainingMinutes() {
        return Math.max(this.minutesOnCard, 0);
    }

    public int getCentsPerMin() {
        return centsPerMin;
    }

    public void setCentsPerMin(int centsPerMin) {
        this.centsPerMin = centsPerMin;
    }

    public void setMinutesOnCard(int dollar) {
        this.minutesOnCard += (dollar * 100) / centsPerMin;
    }

    public static void main(String[] args) {

        CallingCard card = new CallingCard(17); // this card costs 5 cents / minute to use

        card.addDollars(3); // 200 cents / 5 cents/minute = 40 minutes added
        System.out.println(card.getRemainingMinutes()); // returns 40

    }

}
