
package com.galvanize;

public class CellPhone {
    private CallingCard card;
    private boolean isActive;
    private StringBuilder history;
    private int callLength;
    private String currPhoneNumber;
    private boolean cutOff;

    public CellPhone(CallingCard card) {
        this.card = card;
        this.isActive = false;
        this.history = new StringBuilder();
        this.callLength = 0;
        this.currPhoneNumber = null;
        this.cutOff = false;
    }

    public void call(String phoneNumber) {
        setActive(true);
        setCurrPhoneNumber(phoneNumber);
    }

    public void endCall() {
        boolean plural = false;
        setActive(false);
        if (this.history.length() > 0) {
            plural = true;
        }

        if (plural) history.append(", ");
        if (cutOff) {
            if (this.callLength == 1) {
                history.append(String.format("%s (cut off at %s minute)", this.currPhoneNumber, this.callLength));
            } else {
                history.append(String.format("%s (cut off at %s minutes)", this.currPhoneNumber, this.callLength));
            }
        } else {
            if (this.callLength == 1) {
                history.append(String.format("%s (%s minute)", this.currPhoneNumber, this.callLength));
            } else {
                history.append(String.format("%s (%s minutes)", this.currPhoneNumber, this.callLength));
            }
        }

    }

    public void tick() {
        // must check if this.card.getRemainingMinutes() > 0;
        // simulate a minute going by
        //
        if (this.isTalking()) {

            if (this.card.getRemainingMinutes() <= 1) {

                this.card.useMinutes(1);
                setCutOff(true);
                this.endCall();
            } else {

                setCallLength(getCallLength() + 1);
                // TODO : ensure useMinutes affects the current amount of dollars on the card
                this.card.useMinutes(1);
            }
        } else {

            System.out.println("There is no active call \n");
        }
    }

    public boolean isCutOff() {
        return cutOff;
    }

    public void setCutOff(boolean cutOff) {
        this.cutOff = cutOff;
    }

    public String getCurrPhoneNumber() {
        return currPhoneNumber;
    }

    public void setCurrPhoneNumber(String currPhoneNumber) {
        this.currPhoneNumber = currPhoneNumber;
    }

    public int getCallLength() {
        return callLength;
    }

    public void setCallLength(int callLength) {
        this.callLength = callLength;
    }

    public CallingCard getCard() {
        return card;
    }

    public void setCard(CallingCard card) {
        this.card = card;
    }

    public boolean isTalking() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public StringBuilder getHistory() {
        return history;
    }

    public void setHistory(StringBuilder history) {
        this.history = history;
    }
}
